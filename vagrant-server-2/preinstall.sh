#!/bin/sh

#Download JAVA
cd /opt
mkdir jdk
cd jdk
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u4-b20/jdk-7u4-linux-x64.tar.gz"
sudo tar xf jdk-7u4-linux-x64.tar.gz

#Create sym. links for JAVA
cd /usr/bin/
sudo ln -s /opt/jdk/jdk1.7.0_04/bin/java java
sudo ln -s /opt/jdk/jdk1.7.0_04/bin/javac javac
sudo ln -s /opt/jdk/jdk1.7.0_04/bin/javaws javacws

#Download, install and start JBoss
cd /opt
mkdir JBoss
cd JBoss
wget http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.tar.gz
tar xfvz jboss-as-7.1.1.Final.tar.gz
mv jboss-as-7.1.1.Final /usr/local/share/jboss
sudo adduser appserver
sudo chown -R appserver /usr/local/share/jboss
su appserver
cd /usr/local/share/jboss/bin
./add-user.sh appuser apppass
./standalone.sh -Djboss.bind.address=192.168.20.20 -Djboss.bind.address.management=192.168.20.20&

#Open port 8080
sudo iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
sudo ufw reload

#Backup directory
cd /opt
sudo mkdir backup
sudo chown -R appserver:appserver backup

#Reset password for appserver
sudo echo -e "appserver\nappserver" | passwd appserver